package ProjectActivities;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;

public class Activity2 {
	WebDriver driver;
	@BeforeTest
	public void beforeMethod() {
	        //Create a new instance of the Firefox driver
	        driver = new FirefoxDriver();
	        //Open the browser
	        driver.get("http://alchemy.hguy.co/crm");
	}
	@Test
	public void headerImgURL(){
		
		/*
		 * String imgURL =
		 * driver.findElement(By.xpath("//img[@alt='SuiteCRM']")).getText();
		 */
		
		String imgURL = driver.findElement(By.xpath("//img[@alt='SuiteCRM']")).getAttribute("src");
		System.out.println("URL of the hearder image is " +imgURL);
	}

	public void afterMethod() {
        //Close the browser
        driver.close();
}

}
