package ProjectActivities;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity9 {
	WebDriver driver;
	WebDriverWait wait;
	Actions builder;

	@BeforeClass
	public void beforeClass() {
		// Create a new instance of the Firefox driver
		driver = new FirefoxDriver();

		// Open browser
		driver.get("http://alchemy.hguy.co/crm");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}

	@Test
	public void Login() {
		driver.findElement(By.xpath("//input[@id='user_name']")).sendKeys("admin");
		driver.findElement(By.xpath("//input[@id='username_password']")).sendKeys("pa$$w0rd");
		driver.findElement(By.xpath("//input[@id='bigbutton']")).click();

	}

	@Test
	public void popupRead() throws InterruptedException {

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		WebElement salesMenu = driver.findElement(By.id("grouptab_0"));
		Thread.sleep(100);
		Actions builder = new Actions(driver);
		builder.moveToElement(salesMenu).build().perform();

		WebElement leadsSubmenu = driver.findElement(By.xpath(
				"//ul[@class='nav navbar-nav']//li[2]//ul[@class='dropdown-menu']//li[5]//a[@id='moduleTab_9_Leads']"));
		leadsSubmenu.click();
		Thread.sleep(10000);
		for (int i = 1; i <= 10; i++) {
			WebElement name = driver
					.findElement(By.xpath("//div[@class='list-view-rounded-corners']/table/tbody/tr[" + i + "]/td[3]"));
			WebElement userName = driver
					.findElement(By.xpath("//div[@class='list-view-rounded-corners']/table/tbody/tr[" + i + "]/td[8]"));
			System.out.println(name.getText() + " " + userName.getText());

		}

	}

	@AfterClass
	public void aferclass() {
		driver.close();
	}

}
