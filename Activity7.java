package ProjectActivities;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity7 {
	WebDriver driver;
	WebDriverWait wait;

	@BeforeClass
	public void beforeClass() {
		// Initialization of the FirefoxDriver
		driver = new FirefoxDriver();
		wait = new WebDriverWait(driver, 10);

		// Launch FireFox Browser and redirect to Base Suite CRM URL
		driver.get("http://alchemy.hguy.co/crm");
		// Maximizes the browser window
		driver.manage().window().maximize();
		//Waiting for the browse to open
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

	}

	@Test(priority = '0')
	public void homePage() {
		// getting the title of the page
		System.out.println("Title of the page:  " + driver.getTitle());

		// finding and passing the user name and password and clicking on LogIn button
		driver.findElement(By.id("user_name")).sendKeys("admin");
		driver.findElement(By.id("username_password")).sendKeys("pa$$w0rd");
		driver.findElement(By.id("bigbutton")).click();
	}

	@Test(priority = '1')
	public void salesList() {
		// Finding the sales in the navigation bar
		WebElement salesMenu = driver.findElement(By.id("grouptab_0"));
		System.out.println("Navigation Menu Name :  " + salesMenu.getText());

		// Declaring the aciton class for the mouse over
		Actions action = new Actions(driver);
		action.moveToElement(salesMenu).click().perform();

		// Finding Sales Menu list items
		List<WebElement> salesList = driver.findElements(By.xpath("//li[2]//span[2]//ul[1]"));

		// Nested for loop to Display the entire Sales Menu list
		for (WebElement sales : salesList) {
			System.out.println("Sales menu list: " + sales.getText());
		}
	}

	@Test(priority = '2')
	public void leadsListValidation() throws InterruptedException {

		// Finding the Leads WebElement
		WebElement leadValue = driver.findElement(By.id("moduleTab_9_Leads"));
		System.out.println("Text on the leads : " + leadValue.getText());
//		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
//      Clicking on the Lead web element
		leadValue.click();
//		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);		
		Thread.sleep(10000);
		// Finding the Additional information icon element
		driver.findElement(By.xpath("(//span[@class='suitepicon suitepicon-action-info'])[1]")).click();
		// Finding and Passing the Mobile Number in to the string
		String infoIcon = driver.findElement(By.xpath("//span[@class='phone']")).getText();
		// Printing the Mobile Phone Number
		System.out.println("Mobile Number on the additional Details:  " + infoIcon);
	}

	@AfterClass
	public void afterClass() {
		// close the browser
		driver.close();
	}

}
