package ProjectActivities;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.FileNotFoundException;
import java.io.FileReader;
import com.opencsv.CSVReader;


public class Activity11 {
	WebDriver driver;
	WebDriverWait wait;
	Actions builder;

	@BeforeClass
	public void beforeClass() {
		// Create a new instance of the Firefox driver
		driver = new FirefoxDriver();
		//driver=new ChromeDriver();
		// Open browser
		driver.get("http://alchemy.hguy.co/crm");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}

	@Test(priority='0')
	public void Login() throws FileNotFoundException {
		driver.findElement(By.xpath("//input[@id='user_name']")).sendKeys("admin");
		driver.findElement(By.xpath("//input[@id='username_password']")).sendKeys("pa$$w0rd");
		driver.findElement(By.xpath("//input[@id='bigbutton']")).click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		 WebElement salesMenu = driver.findElement(By.id("grouptab_0"));
		 driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		 Actions builder = new Actions(driver);
		 builder
		 .moveToElement(salesMenu).build().perform();
		 
		 CSVReader file = new CSVReader(new FileReader("C:\\Users\\GowthamiDintakurthi\\Desktop\\Leads_Selenium.csv"));
		 
		 WebElement accountsSubmenu = driver.findElement(By.xpath("//*[@id='moduleTab_9_Leads']"));
		 accountsSubmenu.click();
		 
				 //driver.findElement(By.xpath("//ul[@class='nav navbar-nav']//li[2]//ul[@class='dropdown-menu']//li[5]//a[@id='moduleTab_9_Leads']"));
		 driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		 System.out.println("Inside leads");
		 driver.findElement(By.xpath("//div[@id='actionMenuSidebar']/ul/li[4]//div[2]")).click();
		 WebElement uploadInput = driver.findElement(By.id("userfile"));
		 uploadInput.sendKeys("C:\\Users\\GowthamiDintakurthi\\Desktop\\Leads_Selenium.csv");
		 driver.findElement(By.id("gonext")).click();
		 driver.findElement(By.id("gonext")).click();
		 driver.findElement(By.id("gonext")).click();
		 driver.findElement(By.id("importnow")).click();
		 driver.findElement(By.id("finished")).click();
		 driver.findElement(By.xpath("//div[contains(text(),'View Leads')]"));
		 String name = driver.findElement(By.xpath("//*[@field='name']")).getText();
		 Assert.assertEquals(name, "Allene A Santucci");
		 
		 System.out.println("Assertion successful");
		 
		 
		 
		//driver.findElement(By.xpath("//div[contains(text(),'Import Leads')]"));
		// System.out.println("Text in import lead is " + importLead);
		 /*
		 * li.actionmenulinks:nth-child(5) > a:nth-child(1) > div:nth-child(2)
		 * html.yui3-js-enabled body div#ajaxHeader
		 * div#sidebar_container.container-fluid.sidebar_container div.sidebar
		 * div#actionMenuSidebar.actionMenuSidebar ul li.actionmenulinks a
		 * div.actionmenulink /html/body/div[3]/div/div/div[1]/ul/li[4]/a/div[2]
		 */
		
		
	}

	@AfterClass 
	public void aferclass() {
	driver.close();
		  }

}
