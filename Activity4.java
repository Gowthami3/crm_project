package ProjectActivities;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;

public class Activity4 {
	WebDriver driver;
	@BeforeTest
	public void beforeMethod() {
	        //Create a new instance of the Firefox driver
	        driver = new FirefoxDriver();
	        //Open the browser
	        driver.get("http://alchemy.hguy.co/crm");
	}

	@Test
	public void f() {
		
		driver.findElement(By.xpath("//input[@id='user_name']")).sendKeys("admin");
		driver.findElement(By.xpath("//input[@id='username_password']")).sendKeys("pa$$w0rd");
		driver.findElement(By.xpath("//input[@id='bigbutton']")).click();
		
	}
	//*[@id="admin_options"]

	@AfterTest
	public void afterMethod() {
	        //Close the browser
	       driver.close();
	}

	

}
