package ProjectActivities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Activity1 {
	WebDriver driver;
	@BeforeTest
	public void beforeMethod() {
	        //Create a new instance of the Firefox driver
	        driver = new FirefoxDriver();
	        //Open the browser
	        driver.get("http://alchemy.hguy.co/crm");
	}
	@Test
	public void websiteTitle() {
		//String pageTitle = driver.findElement(By.xpath("\\img"))
		String title = driver.getTitle();
		System.out.println("Tilte of the page " + title);
		Assert.assertEquals(title , "SuiteCRM");
		System.out.println("Expected and actual are same");
	}
	@AfterTest
	public void afterMethod() {
	        //Close the browser
	        driver.close();
	}

	
}
