package ProjectActivities;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity13 {
	WebDriver driver;
	WebDriverWait wait;
	Actions builder;
	JavascriptExecutor js;

	@BeforeClass
	public void beforeClass() {
		// Create a new instance of the Firefox driver
		driver = new FirefoxDriver();
		js = (JavascriptExecutor) driver;
		wait = new WebDriverWait(driver, 30);
		// driver=new ChromeDriver();
		// Open browser
		driver.get("http://alchemy.hguy.co/crm");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}

	@Test(priority = '0')
	public void Login() throws IOException {
		driver.findElement(By.xpath("//input[@id='user_name']")).sendKeys("admin");
		driver.findElement(By.xpath("//input[@id='username_password']")).sendKeys("pa$$w0rd");
		driver.findElement(By.xpath("//input[@id='bigbutton']")).click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		WebElement salesMenu = driver.findElement(By.id("grouptab_5"));
		salesMenu.click();
		Actions builder = new Actions(driver);
		builder.moveToElement(salesMenu).build().perform();
		/*
		 * WebElement productView =
		 * driver.findElement(By.xpath("a[contains(text(),'Products')]"));///html/body/
		 * div[2]/nav/div/div[2]/ul/li[7]/span[2]/ul/li[25]/a
		 * js.executeScript("arguments[0].scrollIntoView(true);",productView);
		 * ((JavascriptExecutor)
		 * driver).executeScript("javascript:window.scrollBy(0,250)"); //
		 * ((JavascriptExecutor)
		 * driver).executeScript("arguments[0].scrollIntoView(true);", productView);
		 * productView.click();
		 */

		WebElement navBarValue = driver.findElement(By.id("grouptab_5"));

		System.out.println("Name of the Navigation Item:  " + navBarValue.getText());
		// Name of the Activities value passing into the string
		String allList = navBarValue.getText();

		// Defining the action class for the mouse over activity
		Actions action = new Actions(driver);
		action.moveToElement(navBarValue).click().perform();
		List<WebElement> allList1 = driver.findElements(By.xpath("//li[7]//span[2]//ul[1]"));

		// ul[@class='dropdown-menu']

		for (WebElement webElement : allList1) {

			System.out.println("List of the menu activities: " + webElement.getText());

		}
		/*
		 * wait.until(ExpectedConditions.visibilityOf(driver.
		 * findElement(By.xpath("//a[contains(text(),'Products')]")))).click();
		 * WebElement productView =
		 * driver.findElement(By.xpath("//a[contains(text(),'Products')]"));
		 * ((JavascriptExecutor)driver).executeScript(
		 * "arguments[0].scrollIntoView(true);", productView); productView.click();
		 * System.out.println("product view text is  "+productView);
		 */

		String value = "Products";
		List<WebElement> ele = driver.findElements(By.xpath("//div[@id='toolbar']/ul/li[7]/span[2]/ul/li"));
		for (WebElement ele1 : ele) {
			System.out.println(ele1.getText());
			if (value.equalsIgnoreCase(ele1.getText())) {
				js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].scrollIntoView();", ele1);
				ele1.click();
				break;
			}
		}

		
		  driver.findElement(By.xpath("//div[contains(text(),'Create Product')]")).
		  click(); driver.findElement(By.id("name")).sendKeys("Laptop");
		  
		  driver.findElement(By.id("price")).sendKeys("60000");
		  driver.findElement(By.id("SAVE")).click();
		 
		driver.findElement(By.xpath("//div[contains(text(),'View Products')]")).click();
		XSSFWorkbook productWorkbook = new XSSFWorkbook();
        XSSFSheet productSheet = productWorkbook.createSheet("Product Details");
		
        //WebElement productTableHeader = driver.findElement(By.xpath("/html/body/div[4]/div/div[3]/form[2]/div[3]/table/thead"));
		/*
		 * List<WebElement> header_table = driver.findElements(By.xpath(
		 * "/html/body/div[4]/div/div[3]/form[2]/div[3]/table/thead/tr[1]")); int
		 * rowCount = 0; Row headerRow = productSheet.createRow(rowCount); for (int
		 * column = 0; column < header_table.size(); column++) { int columnCount = 0;
		 * Cell cell = headerRow.createCell(++columnCount); // To retrieve text from the
		 * cells. String celltext = header_table.get(column).getText();
		 * System.out.println("Cell Value Of row number " + header_table.get(column) +
		 * " and column number " + column + " Is " + celltext);
		 * cell.setCellValue(celltext); }
		 */
        // To locate table.
		WebElement productTable = driver.findElement(By.xpath("/html/body/div[4]/div/div[3]/form[2]/div[3]/table/tbody"));
		
		// To locate rows of table.
		List<WebElement> rows_table = productTable.findElements(By.tagName("tr"));
		
		// To calculate no of rows In table.
		int rows_count = rows_table.size();
		System.out.println(rows_count);
		
		// Loop will execute for all the rows of the table
		for (int row = 0; row < rows_count; row++) {
			
			int rowCount = 0;
			Row productRow = productSheet.createRow(++rowCount );
			int columnCount = 0;
			
			// To locate columns(cells) of that specific row.
			List<WebElement> Columns_row = rows_table.get(row).findElements(By.tagName("td"));

			// To calculate no of columns(cells) In that specific row.
			int columns_count = Columns_row.size();
			System.out.println("Number of cells In Row " + row + " are " + columns_count);

			// Loop will execute till the last cell of that specific row.
			for (int column = 2; column < columns_count; column++) {
				Cell cell = productRow.createCell(++columnCount);
				// To retrieve text from the cells.
				String celltext = Columns_row.get(column).getText();
				System.out.println("Cell Value Of row number " + row + " and column number " + column + " Is " + celltext);
				cell.setCellValue(celltext);
			}
		}
		 try (FileOutputStream outputStream = new FileOutputStream("Product Details.xlsx")) {
			 productWorkbook.write(outputStream);
	        }
	}

	@AfterClass
	public void aferclass() {
		// driver.close();
	}

}
