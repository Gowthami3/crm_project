package ProjectActivities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Activity5 {
	WebDriver driver;
	@BeforeTest
	public void beforeMethod() {
	        //Create a new instance of the Firefox driver
	        driver = new FirefoxDriver();
	        //Open the browser
	        driver.get("http://alchemy.hguy.co/crm");
	}

	@Test
	public void f() {
		
		driver.findElement(By.xpath("//input[@id='user_name']")).sendKeys("admin");
		driver.findElement(By.xpath("//input[@id='username_password']")).sendKeys("pa$$w0rd");
		driver.findElement(By.xpath("//input[@id='bigbutton']")).click();
		String navigationMenuColor = driver.findElement(By.xpath("//div[@class='desktop-toolbar']")).getCssValue("color");
		System.out.println("Color of Navigation menu after login is " +navigationMenuColor);
	}
	//*[@id="admin_options"]

	@AfterTest
	public void afterMethod() {
	        //Close the browser
	       driver.close();
	}
}
