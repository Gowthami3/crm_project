package ProjectActivities;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class Activity12 {
	WebDriver driver;
	WebDriverWait wait;
	Actions builder;
	
	@BeforeClass
	public void beforeClass() {
		// Create a new instance of the Firefox driver
		driver = new FirefoxDriver();
		//driver=new ChromeDriver();
		// Open browser
		driver.get("http://alchemy.hguy.co/crm");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}

	@Test(priority='0')
	public void Login() throws FileNotFoundException {
		driver.findElement(By.xpath("//input[@id='user_name']")).sendKeys("admin");
		driver.findElement(By.xpath("//input[@id='username_password']")).sendKeys("pa$$w0rd");
		driver.findElement(By.xpath("//input[@id='bigbutton']")).click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		WebElement salesMenu = driver.findElement(By.id("grouptab_3"));
		 Actions builder = new Actions(driver);
		 builder
		 .moveToElement(salesMenu).build().perform();
		 driver.findElement(By.xpath("//*[@id='moduleTab_9_Meetings']")).click();
		 driver.findElement(By.xpath("//div[contains(text(),'Schedule Meeting')]")).click();
		 WebElement meetingName = driver.findElement(By.id("name"));
		 WebDriverWait wait = new WebDriverWait(driver, 10);
		 wait.until(ExpectedConditions.elementToBeClickable(meetingName)).click();
		 meetingName.sendKeys("TestSchedule");
		 driver.findElement(By.id("search_first_name")).sendKeys("Allene A");
		 driver.findElement(By.id("invitees_search")).click();
		 driver.findElement(By.id("invitees_add_1")).click();
		 driver.findElement(By.id("search_first_name")).sendKeys("Darlene C");
		 driver.findElement(By.id("invitees_search")).click();
		 driver.findElement(By.id("invitees_add_1")).click();
		 driver.findElement(By.id("save_and_send_invites_header")).click();
		 driver.findElement(By.xpath("//div[contains(text(),'View Meetings')]"));
		 String name = driver.findElement(By.xpath("//*[@field='name']")).getText();
		 Assert.assertEquals(name, "TestSchedule");
		 
		 System.out.println("Assertion successful");
	}		 


	@AfterClass 
	public void aferclass() {
		 driver.close();
		  }

}
