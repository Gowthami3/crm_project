package ProjectActivities;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;

public class Activity3 {
	WebDriver driver;
	@BeforeTest
	public void beforeMethod() {
	        //Create a new instance of the Firefox driver
	        driver = new FirefoxDriver();
	        //Open the browser
	        driver.get("http://alchemy.hguy.co/crm");
	}

	@Test
	public void f() {
		
		String firstCopyRight = driver.findElement(By.xpath("//a[@id='admin_options']")).getAttribute("text");
		System.out.println("First copy right in the footer is " +firstCopyRight);
		
	}
	//*[@id="admin_options"]

	@AfterTest
	public void afterMethod() {
	        //Close the browser
	       driver.close();
	}

	

}
