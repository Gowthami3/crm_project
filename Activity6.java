package ProjectActivities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Activity6 {
	WebDriver driver;
	@BeforeTest
	public void beforeMethod() {
	        //Create a new instance of the Firefox driver
	        driver = new FirefoxDriver();
	        //Open the browser
	        driver.get("http://alchemy.hguy.co/crm");
	}

	@Test
	public void f() {
		
		driver.findElement(By.xpath("//input[@id='user_name']")).sendKeys("admin");
		driver.findElement(By.xpath("//input[@id='username_password']")).sendKeys("pa$$w0rd");
		driver.findElement(By.xpath("//input[@id='bigbutton']")).click();
		String activityExist = driver.findElement(By.xpath("//a[@id='grouptab_3']")).getAttribute("text");
		System.out.println("Navigation bar has " +activityExist);
		Assert.assertEquals(activityExist, "Activities");
		System.out.println("Activities exist");
	}
	//*[@id="admin_options"]

	@AfterTest
	public void afterMethod() {
	        //Close the browser
	       driver.close();
	}
}
