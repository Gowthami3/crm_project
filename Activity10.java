package ProjectActivities;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity10 {
	WebDriver driver;
	WebDriverWait wait;
	Actions builder;

	@BeforeClass
	public void beforeClass() {
		// Create a new instance of the Firefox driver
		driver = new FirefoxDriver();

		// Open browser
		driver.get("http://alchemy.hguy.co/crm");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}

	@Test(priority='0')
	public void Login() {
		driver.findElement(By.xpath("//input[@id='user_name']")).sendKeys("admin");
		driver.findElement(By.xpath("//input[@id='username_password']")).sendKeys("pa$$w0rd");
		driver.findElement(By.xpath("//input[@id='bigbutton']")).click();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

	}

	@Test(priority='1')
	  public void homepage() {
		List <WebElement> name=driver.findElements(By.xpath(("//td[@class='dashlet-title']//h3//span[2]")));
	System.out.println("number of dashlet= "+ name.size());
	
	for (WebElement webElement : name) {
				System.out.println(webElement.getText());
				
				
			}
		}
	@AfterClass 
	public void aferclass() {
		  driver.close();
		  }


}
